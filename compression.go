package middleware

import (
	"net/http"

	"github.com/gorilla/handlers"
)

//CompressionMiddleware applies gzip compression to requests
type CompressionMiddleware struct {
}

//Middleware is the handler for compression
func (cmw *CompressionMiddleware) Middleware(next http.Handler) http.Handler {
	return handlers.CompressHandler(next)
}
