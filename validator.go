package middleware

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

//ValidatorMiddleWare validates the content typs of a request
type ValidatorMiddleWare struct {
	acceptedContent map[string][]string
	bypassRoutes    map[string]bool
}

//SetAcceptedContent sets the accepted content for a route.
func (vmw *ValidatorMiddleWare) SetAcceptedContent(accepted map[string][]string) {
	vmw.acceptedContent = accepted
}

//SetValidationBypass sets routes that can be bypassed. For example, healthcheck routes
func (vmw *ValidatorMiddleWare) SetValidationBypass(bypassRoutes map[string]bool) {
	vmw.bypassRoutes = bypassRoutes
}

//Middleware is the handler for content type validation
func (vmw *ValidatorMiddleWare) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routeName := mux.CurrentRoute(r).GetName()

		//ignore gets
		if r.Method == http.MethodGet {
			next.ServeHTTP(w, r)
			return
		}
		//safety check since this should be optional functionality
		if val, ok := vmw.bypassRoutes[routeName]; ok {
			if val {
				next.ServeHTTP(w, r)
				return
			}
		}
		success := validate(r, vmw.acceptedContent[routeName])
		if !success {
			http.Error(w, "TMXValidationMiddleware: content type not accepted", http.StatusNotAcceptable)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func validate(r *http.Request, acceptedContent []string) (success bool) {
	return findStringInSlice(r.Header.Get("content-type"), acceptedContent)
}

func findStringInSlice(searchText string, slice []string) bool {
	for _, foundText := range slice {
		if strings.EqualFold(searchText, foundText) {
			return true
		}
	}
	return false
}
